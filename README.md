Configurations Neovim

1. clone the repository https://github.com/junegunn/vim-plug to the directory created in ~/.config/nvim/ in this path

'curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    
2. Next install all plugins with command in archive init.vim

':PlugInstall'